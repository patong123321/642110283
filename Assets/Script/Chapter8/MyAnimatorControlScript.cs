using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Patipat.GameDev3.Chapter8
{
    public class MyAnimatorControlScript : MonoBehaviour
    {
        protected Animator m_Animator;

        private static readonly int Punch = Animator.StringToHash("Punch");
        private static readonly int Dancing = Animator.StringToHash("Dancing");
        private static readonly int State = Animator.StringToHash("State");
        private static readonly int Turn = Animator.StringToHash("Turn");

        private void Start()
        {
            m_Animator = GetComponent<Animator>();
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                m_Animator.SetTrigger("Jump");
            }

            if (Input.GetKeyDown(KeyCode.Z))
            {
                m_Animator.SetTrigger(Punch);
                if (Input.GetKeyDown(KeyCode.X))
                {
                    m_Animator.SetBool(Dancing, true);
                }

                if (Input.GetKeyDown(KeyCode.C))
                {
                    m_Animator.SetInteger(State, 2);
                }

                if (Input.GetKeyDown(KeyCode.V))
                {
                    m_Animator.SetInteger(Turn, 1);
                }
            }
        }
    }
}