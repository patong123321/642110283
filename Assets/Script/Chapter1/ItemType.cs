using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Patipat.GameDev3.Chapter1
{
    public enum ItemType
    {
        COIN,
        BIGCOIN,
        POWERUP,
        POWERDOWN,
        REGULARCOIN,
        UNREGULARCOIN,
    }
}