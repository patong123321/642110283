using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Patipat.GameDev3.Chapter6.UnityEvents
{
    public class Scale : MonoBehaviour
    {
        [SerializeField] private float Multipy;
        // Start is called before the first frame update
        public void ScaleBig(GameObject actor)
        {
            Vector3 CurrentScale = actor.transform.localScale;
            Vector3 newscale = CurrentScale * Multipy;
            actor.transform.localScale = newscale;
        }
        public void ScaleSmall(GameObject actor)
        {
            Vector3 CurrentScale = actor.transform.localScale;
            Vector3 newscale = CurrentScale / Multipy;
            actor.transform.localScale = newscale;
        }
    }
}