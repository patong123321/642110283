using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class OnPushable : MonoBehaviour
{
    [SerializeField] private float _forceMagnitude = 10;
    private Rigidbody _rigidbody;

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    public void Push(GameObject actor)
    {
        _rigidbody.AddForce(actor.transform.forward*_forceMagnitude,ForceMode.Impulse);
    }
}
